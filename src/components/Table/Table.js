import React, { useState } from "react";
import { AgGridColumn, AgGridReact } from "ag-grid-react";

import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";

import data from "../../data/sensor_readings.json";

export const Table = () => {
  const [displayData] = useState(data);

  return (
    <>
      <h1 style={{ textAlign: "center" }}>Environmental Sensors Table</h1>
      <div className="ag-theme-alpine" style={{ height: 800, width: 1800 }}>
        <AgGridReact rowData={displayData} suppressColumnVirtualisation={true}>
          <AgGridColumn field="id" />
          <AgGridColumn field="box_id" />
          <AgGridColumn field="sensor_type" sortable={true} />
          <AgGridColumn field="unit" />
          <AgGridColumn field="name" sortable={true} />
          <AgGridColumn field="range_l" />
          <AgGridColumn field="range_u" />
          <AgGridColumn field="longitude" />
          <AgGridColumn field="latitude" />
          <AgGridColumn field="reading" />
          <AgGridColumn field="reading_ts" sortable={true} />
        </AgGridReact>
      </div>
    </>
  );
};
