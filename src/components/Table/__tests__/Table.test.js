import React from "react";
import { render, fireEvent } from "@testing-library/react";

import { Table } from "../Table";

// const mockSetState = jest.fn();

// const displayData = [
//   {
//     id: "Box-A2-O3",
//     box_id: "Box-A2",
//     sensor_type: "O3",
//     unit: "ppm",
//     name: "Ozone",
//     range_l: 0.0,
//     range_u: 1000.0,
//     longitude: -0.06507,
//     latitude: 51.51885,
//     reading: 273,
//     reading_ts: "2019-09-10T00:00:00",
//   },
//   {
//     id: "Box-A2-NO2",
//     box_id: "Box-A2",
//     sensor_type: "NO2",
//     unit: "ppm",
//     name: "Nitrogen dioxide",
//     range_l: 0.0,
//     range_u: 1000.0,
//     longitude: -0.06507,
//     latitude: 51.51885,
//     reading: 601,
//     reading_ts: "2019-09-10T00:00:00",
//   },
//   {
//     id: "Box-A2-CO",
//     box_id: "Box-A2",
//     sensor_type: "CO",
//     unit: "ppm",
//     name: "Carbon monoxide",
//     range_l: 0.0,
//     range_u: 1000.0,
//     longitude: -0.06507,
//     latitude: 51.51885,
//     reading: 378,
//     reading_ts: "2019-09-10T00:00:00",
//   },
// ];
// jest.mock("react", () => ({
//   useState: (displayData) => [displayData, mockSetState],
// }));

describe("<Table/>", () => {
  it("renders Table component", () => {
    const { baseElement } = render(<Table />);
    expect(baseElement).toMatchSnapshot();
  });
  it("sorts the Table component by sensor_type", () => {
    const { baseElement, getByText } = render(<Table />);

    fireEvent.click(getByText("Sensor_type"));

    expect(baseElement).toMatchSnapshot();
  });
  it("sorts the Table component by name", () => {
    const { baseElement, getByText } = render(<Table />);
    fireEvent.click(getByText("Name"));

    expect(baseElement).toMatchSnapshot();
  });
  it("sorts the Table component by reading_ts", () => {
    const { baseElement, getByText } = render(<Table />);
    fireEvent.click(getByText("Reading_ts"));

    expect(baseElement).toMatchSnapshot();
  });
});
